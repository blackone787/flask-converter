import os
from flask import Flask
from flask import jsonify, render_template
from converter.integer_to_english import IntegerToEnglish

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<int:number>', methods=['GET'])
def get_word_for_number(number):
    try:
        number_to_word = IntegerToEnglish(int(number))
        number_to_word.convert_number_to_word()
        return jsonify(success='true', 
            data=number_to_word.word,
            error='')
    except Exception as ex:
        print ex
        return jsonify(success='false', 
            data='',
            error=str(ex))
if __name__ == '__main__':
    app.debug = True
    app.run()
